﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BLL.Models
{
    public class BuyerModel
    {
        public int BuyerId { get; set; }

        public int EventId { get; set; }

        public string TesterKey { get; set; }

        [Required]
        public string BuyerName { get; set; }

        public virtual EventModel EventModel { get; set; }
    }
}
