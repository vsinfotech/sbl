﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Models
{
    public class EventModel
    {
        public int EventId { get; set; }
        public string Name { get; set; }
        public int TimeoutInSeconds { get; set; }
    }
}
